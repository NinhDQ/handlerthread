package com.example.handlerthread

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.widget.Toast
import com.example.handlerthread.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var handlerThread: HandlerThread
    private lateinit var handler: Handler
    private var resumeCountLoad = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCount.setOnClickListener {
            startHandlerThread()
            postTaskToHandlerThread()
        }
    }

    private fun startHandlerThread(){
        handlerThread = HandlerThread("handlerThread")
        handlerThread.start()
        handler = Handler(handlerThread.looper)
    }

    private fun cancelHandlerThread(){
        handler.removeCallbacks(handlerThread)
        handlerThread.quit()
    }

    private fun postTaskToHandlerThread(){
        handler.post{
            runOnUiThread {
                Toast.makeText(this, "First post from HandlerThread", Toast.LENGTH_SHORT).show()
            }
        }

        handler.post{
            for (i in 0 ..10) {
                try {
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                runOnUiThread{
                    binding.txtNumber.text = i.toString()
                }
            }
        }

        handler.post{
            val length = 300000000
            for (i in 0 until length){
                if (i == length-1){
                    runOnUiThread {
                        binding.txtNumber.text = getString(R.string.finish)
                        Toast.makeText(this, "Second post from HandlerThread", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        resumeCountLoad++
        if (resumeCountLoad > 1)
            startHandlerThread()
    }

    override fun onPause() {
        super.onPause()
        cancelHandlerThread()
    }
}